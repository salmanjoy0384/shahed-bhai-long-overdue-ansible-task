CREATE TABLE users (id int NOT NULL AUTO_INCREMENT, email varchar(100) NOT NULL, password varchar(100) NOT NULL, primary key(id));
CREATE TABLE history (id int NOT NULL AUTO_INCREMENT, number varchar(100) NOT NULL, response varchar(100) NOT NULL, type varchar(30) NOT NULL, time timestamp default current_timestamp, primary key(id));

CREATE USER 'twotieruser'@'%' IDENTIFIED BY 'super-secret-password';
GRANT ALL PRIVILEGES ON *.* TO 'twotieruser'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;